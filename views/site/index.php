<?php

/* @var $this yii\web\View */
/* @var $searchModel \app\models\ApiDataSearch */
/* @var $dataProvider \yii\elasticsearch\ActiveDataProvider */

$this->title = 'My Yii Application';

?>
<div class="col-lg-12">
    <div class="row">
        <div class="col-md-12">
            <?= $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?= \yii\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'showFooter' => true,
                        'columns' => [
                            [
                                'attribute' => 'firstName'
                            ],
                            [
                                'attribute' => 'lastName'
                            ],
                            [
                                'attribute' => 'phoneNumbers',
                                'format' => 'raw',
                                'value' => function(\app\models\ApiDataSearch $model) {
                                    $numbers = trim(\yii\helpers\Json::decode($model->phoneNumbers), '[]');
                                    $numbers = str_replace(',', '<br>', $numbers);
                                    return $numbers;
                                }
                            ],
                            [
                                'attribute' => 'createdAt',
                                'format' => 'datetime'
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
</div>
