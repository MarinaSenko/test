<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\ApiDataSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="form-group">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'post',
        'layout' => 'inline',
     ]); ?>

    <?php echo $form->field($model, 'firstName')->textInput(['placeholder' => 'First name']); ?>
    <?php echo $form->field($model, 'lastName')->textInput(['placeholder' => 'Last name']); ?>

    <div class="form-group">
        <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']); ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
