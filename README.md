## INSTALLATION

### Get source code
##### Clone repository
```
git clone https://MarinaSenko@bitbucket.org/MarinaSenko/test.git
```

### Docker installation

1. Install [docker](https://docs.docker.com/engine/installation/)
and [docker-compose](https://docs.docker.com/compose/install/) to your system

2. Run ``docker-compose up -d``

3. Run ``docker-compose exec phpfpm composer install``

4. Docker host - test.local, make sure it is in /etc/hosts

5. Copy .env.example content to .env