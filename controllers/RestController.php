<?php

namespace app\controllers;

use app\models\ApiData;
use yii\db\ActiveRecord;
use yii\rest\Serializer;
use yii\rest\OptionsAction;
use yii\rest\ActiveController;
use app\controllers\actions\SaveAction;

/**
 * TODO move from this directory (to api, for example)
 *
 * Class RestController
 *
 * @package api\modules\v1\controllers
 *
 * @author  Marina Senko
 */
class RestController extends ActiveController
{

    /**
     * @var string
     *
     * @author Marina Senko
     */
    public $modelClass = ActiveRecord::class;
    
    /**
     * @return array
     *
     * @author Marina Senko
     */
    public function actions(): array
    {
        return [
            'save' => [
                'class' => SaveAction::class,
                'params' => \Yii::$app->request->post(),
                'modelClass' => ApiData::class,
            ],
            'options' => [
                'class' => OptionsAction::class,
            ]
        ];
    }

    /**
     * @return array
     *
     * @author Marina Senko
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::class,
                'only' => ['save'],
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
       ];
    }
}