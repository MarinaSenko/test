<?php

namespace app\controllers;

use yii\web\Controller;
use yii\web\ErrorAction;
use app\models\ApiDataSearch;

/**
 * Class SiteController
 *
 * @package app\controllers
 *
 * @author  Marina Senko
 */
class SiteController extends Controller
{

    /**
     * @return array
     *
     * @author Marina Senko
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    /**
     * @param string $sort
     *
     * @return string
     * @throws \ReflectionException
     *
     * @author Marina Senko
     */
    public function actionIndex($sort = ''): string {

        $searchModel = new ApiDataSearch();
        $modelName = (new \ReflectionClass($searchModel))->getShortName();
        $post = \Yii::$app->request->post();
        $params = $post[$modelName] ?? [];
        $dataProvider = $searchModel->search($params, $sort);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }
}
