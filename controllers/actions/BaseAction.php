<?php

namespace app\controllers\actions;

use yii\rest\Action;

/**
 * TODO move from this directory (to api, for example)
 *
 * Class BaseAction
 *
 * @property array $response
 * @property array $errors
 * @property int $status
 *
 * @package app\controllers\actions
 *
 * @author  Marina Senko
 */
class BaseAction extends Action
{

    /**
     * @var array
     *
     * @author Marina Senko
     */
    public $params;

    /**
     * @var array
     *
     * @author Marina Senko
     */
    private $_errors = [];

    /**
     * @var int
     *
     * @author Marina Senko
     */
    private $_status = 200;

    /**
     * @param array $errors
     *
     * @author Marina Senko
     */
    protected function setErrors(array $errors): void
    {
        $this->_errors = $errors;
    }

    /**
     * @param string $message
     * @param string $attribute
     *
     * @author Marina Senko
     */
    protected function setError(string $message, $attribute = 'default'): void 
    {
        $this->_errors[] = [$attribute => $message];
    }

    /**
     * @param int $status
     *
     * @author Marina Senko
     */
    protected function setStatus($status = 200): void 
    {
        if (!\is_numeric($status)) {
            return;
        }
        $this->_status = $status;
    }

    /**
     * @return array
     *
     * @author Marina Senko
     */
    protected function getResponse(): array 
    {
        $response = [
            'status' => $this->_status
        ];
        
        if ($this->_errors) {
            $response['errors'] = $this->_errors;
        }

        return $response;
    }
}