<?php

namespace app\controllers\actions;

use yii\di\Instance;
use yii\db\ActiveRecord;
use app\queues\SaveDataQueue;

/**
 * TODO move from this directory (to api, for example)
 *
 * Class SaveAction
 *
 * @package app\controllers\actions
 *
 * @author  Marina Senko
 */
class SaveAction extends BaseAction
{


    /**
     * @return array
     *
     * @author Marina Senko
     */
    public function run(): array
    {
        try{
            $model = Instance::ensure($this->modelClass, ActiveRecord::class);
            $model->setAttributes($this->params);
            if(!$model->validate()) {
                $this->setStatus(400);
                $this->setErrors($model->errors);
            } else {
                \Yii::$app->queue->push(new SaveDataQueue([
                    'modelClass' => $this->modelClass,
                    'data' => $model->toArray()
                 ]));
            }
        } catch(\Exception $e) {
            \Yii::error($e->getMessage());
            $this->setStatus(500);
            $this->setError('Internal error');
        }
        
        return $this->getResponse();
    }
}