<?php

namespace app\queues;

use yii\di\Instance;
use yii\base\BaseObject;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;
use yii\queue\JobInterface;

/**
 * Class SaveDataQueue
 *
 * @package app\queues
 *
 * @author  Marina Senko
 */
class SaveDataQueue extends BaseObject implements JobInterface
{

    /**
     * @var array|null
     *
     * @author Marina Senko
     */
    public $data;

    /**
     * @var string|null
     *
     * @author Marina Senko
     */
    public $modelClass;


    /**
     * @param \yii\queue\Queue $queue
     *
     * @author Marina Senko
     */
    public function execute($queue): void
    {
        try{
            /**
             * @var ActiveRecord $model
             */
            $model = Instance::ensure($this->modelClass, ActiveRecord::class);
            $model->setAttributes($this->data);
            if(!$model->save()) {
                $queue->push($this);
                \Yii::error(VarDumper::dumpAsString([
                    'class' => __CLASS__,
                    'method' => __METHOD__,
                    'line' => __LINE__,
                    'errors' => $model->errors,
                ]));
            }
        } catch(\Exception $e) {
            \Yii::error($e->getMessage());
        }
    }
}