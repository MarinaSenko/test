<?php

use yii\db\Migration;

/**
 * Class m190218_121649_create_api_data_table
 *
 * @author Marina Senko
 */
class m190218_121649_create_api_data_table extends Migration
{

    public const TABLE = '{{%api_data}}';

    /**
     * @return bool|void
     *
     * @author Marina Senko
     */
    public function safeUp()
    {
        $tableOptions = '';

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('api_data', [
            'id' => $this->primaryKey(),
            'firstName' => $this->string()->notNull(),
            'lastName' => $this->string()->notNull(),
            'phoneNumbers' => $this->text()->notNull(),
            'createdAt' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * @return bool|void
     *
     * @author Marina Senko
     */
    public function safeDown()
    {
        $this->dropTable('api_data');
    }
}
