<?php

namespace app\models;

use yii\helpers\Json;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use app\behaviors\ElasticSearchBehavior;

/**
 * This is the model class for table "api_data".
 *
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property string $phoneNumbers
 * @property int $createdAt
 */
class ApiData extends ActiveRecord
{
    /**
     * @return string
     *
     * @author Marina Senko
     */
    public static function tableName(): string
    {
        return '{{%api_data}}';
    }

    /**
     * @return array
     *
     * @author Marina Senko
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['createdAt'],
                ],
            ],
            [
                'class' => ElasticSearchBehavior::class,
                'elasticClass' => ApiDataSearch::class
            ],
        ];
    }

    /**
     * @return array
     *
     * @author Marina Senko
     */
    public function rules(): array
    {
        return [
            [['firstName', 'lastName', 'phoneNumbers'], 'required'],
            [['phoneNumbers'], 'string'],
            [['createdAt'], 'integer'],
            [['firstName', 'lastName'], 'string', 'min' => 2, 'max' => 255],
        ];
    }

    /**
     * @return array
     *
     * @author Marina Senko
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'firstName' => 'First name',
            'lastName' => 'Last name',
            'phoneNumbers' => 'Phone numbers',
            'createdAt' => 'Date created',
        ];
    }

    /**
     * @return bool
     *
     * @author Marina Senko
     */
    public function beforeValidate(): bool {

        $this->phoneNumbers = !empty($this->phoneNumbers) ? Json::encode($this->phoneNumbers) : null;

        return parent::beforeValidate();
    }
}
