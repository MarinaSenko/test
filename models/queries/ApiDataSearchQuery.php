<?php

namespace app\models\queries;

use yii\elasticsearch\ActiveQuery;

/**
 * Class ApiDataSearchQuery
 *
 * @package app\models\queries
 *
 * @author  Marina Senko
 */
class ApiDataSearchQuery extends ActiveQuery
{

    /**
     * @param array $data
     *
     * @return ApiDataSearchQuery
     *
     * @author Marina Senko
     */
    public function matchAll(array $data): self {

        $condition = [];

        foreach($data as $attribute => $value) {
            $condition['bool']['must'][] = [
                'match' => [
                    $attribute => $value,
                ],
            ];
        }

        if(!empty($condition)) {
            $this->query($condition);
        }

        return $this;
    }
}