<?php

namespace app\models;

use yii\elasticsearch\ActiveRecord;
use yii\elasticsearch\ActiveDataProvider;
use app\models\queries\ApiDataSearchQuery;

/**
 * Class ApiDataSearch
 *
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property string $phoneNumbers
 * @property int $createdAt
 *
 * @package app\models
 *
 * @author  Marina Senko
 */
class ApiDataSearch extends ActiveRecord
{

    /**
     * @return string
     *
     * @author Marina Senko
     */
    public static function index(): string
    {
        return 'genesis';
    }

    /**
     * @return string
     *
     * @author Marina Senko
     */
    public static function type(): string
    {
        return 'api_data';
    }

    /**
     * @return array
     *
     * @author Marina Senko
     */
    public function attributes(): array
    {
        return [
            'id',
            'firstName',
            'lastName',
            'phoneNumbers',
            'createdAt'
        ];
    }

    /**
     * @return array
     *
     * @author Marina Senko
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'firstName' => 'First name',
            'lastName' => 'Last name',
            'phoneNumbers' => 'Phone numbers',
            'createdAt' => 'Date',
        ];
    }

    /**
     * @return array
     *
     * @author Marina Senko
     */
    public function rules(): array
    {
        return [
            [$this->attributes(), 'safe']
        ];
    }

    /**
     * @return ApiDataSearchQuery
     *
     * @author Marina Senko
     */
    public static function find(): ApiDataSearchQuery
    {
        return new ApiDataSearchQuery(get_called_class());
    }

    /**
     * @param array $params
     * @param string $sort
     *
     * @return ActiveDataProvider
     *
     * @author Marina Senko
     */
    public function search(array $params, string $sort): ActiveDataProvider
    {

        $query = self::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => ['firstName', 'lastName', 'createdAt'],
        ]]);

        $this->setAttributes($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $params = $this->filterAttributes();

        if(!empty($params)) {
            $query->matchAll($params);
        }

        $order = $this->getOrderByParam($sort);

        if(!empty($order)) {
            $query->orderBy($order);
        }

        return $dataProvider;
    }

    /**
     * @param string $sort
     *
     * @return array
     *
     * @author Marina Senko
     */
    protected function getOrderByParam(string $sort): array
    {
        if(!empty($sort)) {
            if(strpos($sort, '-') === 0) {
                return [
                    substr($sort, 1) => SORT_DESC
                ];
            }
            return [
                $sort => SORT_ASC
            ];
        }

        return [];
    }

    /**
     * @return array
     *
     * @author Marina Senko
     */
    protected function filterAttributes(): array
    {
        $params = [];

        foreach($this->attributes as $attribute => $value) {
            if(!empty($value)) {
                $params[$attribute] = $value;
            }
        }

        return $params;
    }
}