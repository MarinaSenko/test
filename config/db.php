<?php

return [
    'class'    => \yii\db\Connection::class,
    'dsn'      => 'mysql:host='.getenv('DB_HOST').';dbname='.getenv('DB_NAME'),
    'username' => getenv('DB_USERNAME'),
    'password' => getenv('DB_PASSWORD'),
    'charset'  => getenv('DB_CHARSET'),
];
