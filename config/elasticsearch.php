<?php

return [
    'class' => \yii\elasticsearch\Connection::class,
    'nodes' => [
        ['http_address' => getenv('ELASTICSEARCH_HOST') . ':' . getenv('ELASTICSEARCH_PORT')],
    ],
];