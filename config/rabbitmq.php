<?php

return [
    'class' => \yii\queue\amqp_interop\Queue::class,
    'host' => getenv('AMQP_HOST'),
    'port' => getenv('AMQP_PORT'),
    'user' => getenv('AMQP_USER'),
    'password' => getenv('AMQP_PASSWORD'),
    'queueName' => 'queue',
    'driver' => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_LIB,
];