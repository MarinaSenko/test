<?php

namespace app\behaviors;

use yii\di\Instance;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord as DbActiveRecord;
use yii\elasticsearch\ActiveRecord as EsActiveRecord;

/**
 * Behavior for elasticsearch indexing
 * Class ElasticSearchBehavior
 *
 * @package app\behaviors
 *
 * @var array $processedData
 *
 * @author  Marina Senko
 */
class ElasticSearchBehavior extends \yii\base\Behavior
{

    /**
     * @var DbActiveRecord|null
     *
     * @author Marina Senko
     */
    public $owner;

    /**
     * @var string|null
     *
     * @author Marina Senko
     */
    public $elasticClass;

    /**
     * @var EsActiveRecord|null
     *
     * @author Marina Senko
     */
    public $elasticModel;


    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!$this->elasticClass) {
            throw new InvalidConfigException("Need to set 'elasticClass' attribute");
        }

        $this->elasticModel = Instance::ensure($this->elasticClass, EsActiveRecord::class);
    }

    /**
     * @return array
     *
     * @author Marina Senko
     */
    public function events(): array
    {
        return [
            DbActiveRecord::EVENT_AFTER_INSERT => 'insert',
            DbActiveRecord::EVENT_AFTER_UPDATE => 'update',
            DbActiveRecord::EVENT_AFTER_DELETE => 'delete',
        ];
    }

    /**
     * Inserting elasticsearch index record
     * @author Marina Senko
     */
    public function insert(): void
    {
        $this->elasticModel->setAttributes($this->owner->attributes);
        $this->elasticModel->insert();
    }

    /**
     * Updating elasticsearch index record
     * @throws \Exception
     *
     * @author Marina Senko
     */
    public function update(): void
    {
        if (!$this->elasticClass::updateAll($this->owner->attributes, ['id' => $this->owner->primaryKey])) {
            $this->insert();
        }
    }

    /**
     * Deleting record from elasticsearch index
     *
     * @author Marina Senko
     */
    public function delete(): void
    {
        $this->elasticClass::deleteAll(['id' => $this->owner->primaryKey]);
    }
}